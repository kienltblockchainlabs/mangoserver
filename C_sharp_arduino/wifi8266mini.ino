#include <SimpleDHT.h>
#include <ESP8266WiFi.h>
// for DHT11, 
//      VCC: 5V or 3V
//      GND: GND
//      DATA: 2

//      Config IFTTT 
String MakerIFTTT_Key ;
String MakerIFTTT_Event;
char *append_str(char *here, String s) {  int i=0; while (*here++ = s[i]){i++;};return here-1;}
char *append_ul(char *here, unsigned long u) { char buf[20]; return append_str(here, ultoa(u, buf, 10));}
char post_rqst[256];char *p;char *content_length_here;char *json_start;int compi;

//
bool sent = false;
bool state = true;
long time_now = 0;
int pinDHT11 = 5;
byte temperature = 0;
byte humidity = 0;
int err = SimpleDHTErrSuccess;
SimpleDHT11 dht11;

//      Wifi Settings
const char* ssid = "AndroidAP";
const char* password = "jtlt0600";

//      ThingSpeak Settings
const int channelID = 337546;
String writeAPIKey = "O4TC0BNLAU3EZI8Q";
const char* server = "api.thingspeak.com";

WiFiClient client;
void setup() {
  Serial.begin(9600);
  //pinMode(LED_BUILTIN, OUTPUT);
  Serial.println();
  Serial.println();
  Serial.print("Ket noi toi mang ");
  Serial.println(ssid);
  WiFi.begin(ssid,password);
  while(WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.println("Da ket noi WiFi");
}

void loop() {
  if (Serial.available()){
    char temp = Serial.read();
    if (temp == '0')
      state = false;
    if (temp == '1')
      state = true;
  }
  // start working...
  if ((err = dht11.read(pinDHT11, &temperature, &humidity, NULL)) != SimpleDHTErrSuccess) {
    Serial.print("Read DHT11 failed, err="); Serial.println(err);delay(1000);
    return;
  }
  else {
    //      Trigger Alarm
    if (int(temperature)>40 && sent == false){
      if (client.connect("maker.ifttt.com",80)) {
         MakerIFTTT_Key ="mbAhCjKmkWWskBNuCSLtb";
         MakerIFTTT_Event ="Overheat";
         p = post_rqst;
         p = append_str(p, "POST /trigger/");
    p = append_str(p, MakerIFTTT_Event);
    p = append_str(p, "/with/key/");
    p = append_str(p, MakerIFTTT_Key);
    p = append_str(p, " HTTP/1.1\r\n");
    p = append_str(p, "Host: maker.ifttt.com\r\n");
    p = append_str(p, "Content-Type: application/json\r\n");
    p = append_str(p, "Content-Length: ");
    content_length_here = p;
    p = append_str(p, "NN\r\n");
    p = append_str(p, "\r\n");
    json_start = p;
    p = append_str(p, "{\"value1\":\"");
    p = append_str(p, "84964246477");
    p = append_str(p, "\",\"value2\":\"");
    p = append_str(p, "Hello Kienbede, overheat cmnr");
    p = append_str(p, "\",\"value3\":\"");
    p = append_str(p, "");
    p = append_str(p, "\"}");

    compi= strlen(json_start);
    content_length_here[0] = '0' + (compi/10);
    content_length_here[1] = '0' + (compi%10);
    client.print(post_rqst);
    sent = true;
      }
    }
    if (state == true){
    //          print Information
    
    time_now = millis();
    Serial.print(time_now);
    Serial.print("|");
    Serial.print(int(temperature));
    Serial.print("|");
    Serial.println(int(humidity));
    }
  }
  //            Send data to ThingSpeak
  if (client.connect(server, 80)) {
                // Construct API request body
                String body = "field1=" + String(float(temperature), 1) + "&field2=" + String(float(humidity), 1);

                client.print("POST /update HTTP/1.1\n");
                client.print("Host: api.thingspeak.com\n");
                client.print("Connection: close\n");
                client.print("X-THINGSPEAKAPIKEY: " + writeAPIKey + "\n");
                client.print("Content-Type: application/x-www-form-urlencoded\n");
                client.print("Content-Length: ");
                client.print(body.length());
                client.print("\n\n");
                client.print(body);
                client.print("\n\n");
  }
  client.stop();
}

