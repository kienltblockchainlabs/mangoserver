var express = require('express');
var path = require('path');
//var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var nodemailer = require('nodemailer');

var dataArr = [];

//index
var mongo = require('mongodb');
var db = require('monk')("trung kien:hockhongchoi97@ds129394.mlab.com:29394/kiendata");
//var db = require('monk')("localhost/dht11");

//var index = require('./routes/index');
var users = require('./routes/users');
var about =  require('./routes/about');
var contact = require('./routes/contact');

var app = express();
var server = require('http').createServer(app);
server.listen(process.env.PORT || 8888);
var io = require('socket.io').listen(server);

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
// app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));


app.get('/', function(req, res, next){
  io.sockets.emit('serverPort', process.env.PORT);
  res.render('index',{title: 'Home'});
});

app.post('/reset', function(req, res, next){
  dataArr = [];
  io.sockets.emit('serverSendData', dataArr);
  res.location('/');
  res.redirect('/');
});

app.post('/activity', function(req, res, next){
  var data = {
    time: parseInt(req.query.time),
    temp: parseInt(req.query.temp),
    humid: parseInt(req.query.humid)
  }
  console.log(data);
  
});

app.get('/dht11', function(req, res, next) {
    
      ///Receive data
      var data = {
        time: parseInt(req.query.time),
        temp: parseInt(req.query.temp),
        humid: parseInt(req.query.humid)
      }
      
      var lastData = [Math.round(data.time/1000), data.temp, data.humid];

      if (data.time && data.temp && data.humid && !isNaN(data.time) && !isNaN(data.temp) && !isNaN(data.humid)){
        if(dataArr.length == 50){
          dataArr.shift();
        }
        dataArr.push(lastData);
        
        ///Send array data back to all client
        //setInterval(function(){io.sockets.emit('serverSendData', dataArr)}, 3000);
        io.sockets.emit('serverSendData', dataArr);

        ///insert to database
        var collection = db.get('dht11'); // db.get('data') if use localhost
        collection.insert(data, function(err, result){
            if(err){
                console.log('There was an issue in submitting data to server');
            }else{
                console.log('Data Submitted To Server');
            }
        });

        ///Send Email Warning
        var transporter = nodemailer.createTransport({
          service: 'Gmail',
          auth: {
            user: 'tien9ian9@gmail.com',
            pass: 'tien9_ian9'
          }
        });
        var mailOptions = {
          from: '<tien9ian9@gmail.com>',
          to: '1511640@hcmut.edu.vn',
          subject: 'Warning about temperature ',
          text: '',
          html: ''
        };

        if(data.temp > 40){
          mailOptions.text = 'Temperature was too high: ' + data.temp;
          mailOptions.html = '<p>Temperature was too high: ' + data.temp + ' &#8451;</p>';

          transporter.sendMail(mailOptions, function(error, info){
            if(error){
              console.log(error);
            }else{
              console.log('Mail message send: '+ info.response);
            }
          });
        }
        if(data.temp < 20){
          mailOptions.text = 'Temperature was too low: ' + data.temp;
          mailOptions.html = '<p>Temperature was too low: ' + data.temp + ' &#8451;</p>';

          transporter.sendMail(mailOptions, function(error, info){
            if(error){
              console.log(error);
            }else{
              console.log('Mail message send: '+ info.response);
            }
          });
        }

      }else{
        console.log('Some data is missed!');
      }
      
  res.render('index'); 
});

// io.sockets.on('connection', function(socket){
//   console.log('co sensor ket noi');
//   io.sockets.emit('serverSendData', dataArr);
// });


//app.use('/', index);
app.use('/users', users);
app.use('/about', about);
app.use('/contact', contact);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;